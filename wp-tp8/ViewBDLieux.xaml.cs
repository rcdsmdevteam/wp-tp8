﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using wp_tp8.ViewModels;
using Newtonsoft.Json.Linq;

namespace wp_tp8
{
    public partial class ViewBDLieux : PhoneApplicationPage
    {
        public ViewBDLieux()
        {
            InitializeComponent();

            DataContext = App.BaseDeDonneesDesLieux;
        }

        private void nouveauLieux_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Lieux unLieux = new Lieux();
            App.BaseDeDonneesDesLieux.LesLieux.Add(unLieux);
        }

        private void deleteLieux_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Lieux unLieux = (Lieux)LBDesLieux.SelectedItem;
            App.BaseDeDonneesDesLieux.LesLieux.Remove(unLieux);
            LBDesLieux.SelectedItem = null;
        }

        private void googleAPI_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            WebClient wcl = new WebClient();
            Lieux leLieux = (Lieux)LBDesLieux.SelectedItem;
            Uri url = new Uri("http://maps.googleapis.com/maps/api/geocode/json?address=" + leLieux.Nom, UriKind.Absolute);
            wcl.DownloadStringAsync(url);
            wcl.DownloadStringCompleted += wcl_DownloadStringCompleted;
        }

        void wcl_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            Lieux leLieux = (Lieux)LBDesLieux.SelectedItem;
            JObject json = JObject.Parse(e.Result);
            try
            {
                leLieux.Lattitude = Double.Parse(json["results"][0]["geometry"]["location"]["lat"].ToString());
                leLieux.Longitude = Double.Parse(json["results"][0]["geometry"]["location"]["lng"].ToString());
            }
            catch (Exception)
            {
                leLieux.Lattitude = 0;
                leLieux.Longitude = 0;
            }
        }

        private void LBDesLieux_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (LBDesLieux.SelectedItem != null)
            {
                googleAPI.Visibility = System.Windows.Visibility.Visible;
                deleteLieux.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                googleAPI.Visibility = System.Windows.Visibility.Collapsed;
                deleteLieux.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
    }
}
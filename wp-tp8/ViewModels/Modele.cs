﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace wp_tp8.ViewModels
{
    public class Modele : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected void ChangeProperty<T>(ref T attribute, T value, [CallerMemberName] String propertyName = "")
        {
            if (!object.Equals(attribute, value))
            {
                attribute = value;
                NotifyPropertyChanged(propertyName);
            }
        }
    }
}

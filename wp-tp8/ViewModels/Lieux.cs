﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wp_tp8.ViewModels
{
    public class Lieux : Modele
    {
        private String nom;

        public String Nom
        {
            get { return nom; }
            set { ChangeProperty(ref nom, value); }
        }

        private Double lattitude;

        public Double Lattitude
        {
            get { return lattitude; }
            set { ChangeProperty(ref lattitude, value); }
        }

        private Double longitude;

        public Double Longitude
        {
            get { return longitude; }
            set { ChangeProperty(ref longitude, value); }
        }

        public Lieux()
        {
            lattitude = 0.0;
            longitude = 0.0;
        }

        public Lieux(String nom, Double lattitude = 0.0, Double longitude = 0.0)
        {
            Nom = nom;
            Lattitude = lattitude;
            Longitude = longitude;
        }

        public override string ToString()
        {
            return Nom;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wp_tp8.ViewModels
{
    public class BDLieux : Modele
    {
        private ObservableCollection<Lieux> lesLieux;

        public ObservableCollection<Lieux> LesLieux
        {
            get { return lesLieux; }
            set { ChangeProperty(ref lesLieux, value); }
        }

        public BDLieux()
        {
            this.LesLieux = new ObservableCollection<Lieux>();
        }
    }
}
